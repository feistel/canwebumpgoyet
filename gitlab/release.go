package gitlab

import "github.com/Masterminds/semver/v3"

type Release struct {
	Version string
	Omnibus *OmnibusRelease
	CNG     *CNGRelease
}

type CNGRelease struct {
	Version   string
	GoVersion *semver.Version
}

type OmnibusRelease struct {
	BuilderImageRevision *semver.Version
	BuilderGoVersion     *semver.Version
}

func SupportedOmnibusVersions() *semver.Constraints {
	c, _ := semver.NewConstraint(">= 1.0.0")
	return c
}

func SupportedGitlabVersions() *semver.Constraints {
	c, _ := semver.NewConstraint(" >= 13.9.0")
	return c
}
