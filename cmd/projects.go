package cwbgy

import (
	"fmt"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/feistel/canwebumpgoyet/gitlab"
)

func (a *App) CheckProjects() error {
	glCh, glErrCh := a.gitlabResolver.Query(gitlab.SupportedGitlabVersions(), 3)

	var (
		versions []<-chan *semver.Version
		errors   []<-chan error
	)

	{
		for _, id := range a.projects {
			goCh, goErrCh := a.golangResolver.FindVersion(id)
			versions = append(versions, goCh)
			errors = append(errors, goErrCh)
		}
	}

	var oldest *semver.Version

	select {
	case err := <-glErrCh:
		return err
	case <-glCh: // latest
		<-glCh      // old
		g := <-glCh // oldest
		oldest = g.CNG.GoVersion
	}

	outdated := false

	for i, vCh := range versions {
		select {
		case err := <-errors[i]:
			return err
		case v := <-vCh:
			// ignore major version, we only support go 1.x
			if oldest.Minor() > v.Minor() {
				a.logger.Printf("\u2717 %s (go %s)", a.projects[i], v)
				outdated = true
			} else if a.verbose {
				a.logger.Printf("\u2713 %s", a.projects[i])
			}
		}
	}

	if !outdated {
		a.logger.Printf("Everything is up to date! Good job!")
	} else {
		return fmt.Errorf("some projects are outdated, please upgrade to %s", oldest)
	}

	return nil

}
