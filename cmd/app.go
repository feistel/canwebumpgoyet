package cwbgy

import (
	"log"
	"net/http"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/feistel/canwebumpgoyet/internal/gitlab"
	"gitlab.com/feistel/canwebumpgoyet/internal/golang"
)

type App struct {
	client         *http.Client
	logger         *log.Logger
	golangResolver *golang.Resolver
	gitlabResolver *gitlab.Resolver
	constraints    *semver.Constraints
	mainBranch     bool
	verbose        bool
	limit          int
	projects       []string
	cng            bool
}

type AppOption func(*App)

func NewApp(opts ...AppOption) *App {
	var a App
	for _, opt := range opts {
		opt(&a)
	}

	goOpt := []golang.Option{
		golang.WithClient(a.client),
		golang.WithLogger(a.logger),
		golang.WithVerbose(a.verbose),
	}

	a.golangResolver = golang.NewResolver(goOpt...)

	gitOpt := []gitlab.Option{
		gitlab.WithClient(a.client),
		gitlab.WithLogger(a.logger),
		gitlab.WithVerbose(a.verbose),
	}

	a.gitlabResolver = gitlab.NewResolver(gitOpt...)

	return &a
}

func WithVerbose(verbose bool) AppOption {
	return func(a *App) {
		a.verbose = verbose
	}
}

func WithLogger(l *log.Logger) AppOption {
	return func(a *App) {
		a.logger = l
	}
}

func WithClient(client *http.Client) AppOption {
	return func(a *App) {
		a.client = client
	}
}

func WithConstraints(constraints *semver.Constraints) AppOption {
	return func(a *App) {
		a.constraints = constraints
	}
}

func WithMainBranch(mainBranch bool) AppOption {
	return func(a *App) {
		a.mainBranch = mainBranch
	}
}

func WithLimit(limit int) AppOption {
	return func(a *App) {
		a.limit = limit
	}
}

func WithProjects(projects ...string) AppOption {
	return func(a *App) {
		a.projects = append(a.projects, projects...)
	}
}

func WithCNG(cng bool) AppOption {
	return func(a *App) {
		a.cng = cng
	}
}
