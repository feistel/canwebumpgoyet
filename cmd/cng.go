package cwbgy

import (
	"fmt"

	"github.com/Masterminds/semver/v3"
)

func (a *App) CheckCNG() error {
	glCh, glErrCh := a.gitlabResolver.QueryBranch("master")
	goCh, goErrCh := a.golangResolver.SupportedVersions()

	var (
		goLatestVersion     *semver.Version
		goAdditionalVersion *semver.Version
	)

	select {
	case err := <-goErrCh:
		return err
	case goLatestVersion = <-goCh:
		goAdditionalVersion = <-goCh
	}

	var CNGGoVersion *semver.Version

	select {
	case err := <-glErrCh:
		return err
	case latest := <-glCh: // latest
		CNGGoVersion = latest.CNG.GoVersion
	}

	if CNGGoVersion.LessThan(goAdditionalVersion) {
		return fmt.Errorf("CNG is using an unsupported version of go (%s), please upgrade to %s", CNGGoVersion, goAdditionalVersion)
	}

	if CNGGoVersion.GreaterThan(goAdditionalVersion) && CNGGoVersion.LessThan(goLatestVersion) {
		return fmt.Errorf("CNG is using an unsupported version of go (%s), please upgrade to %s", CNGGoVersion, goLatestVersion)
	}

	a.logger.Printf("Everything is up to date! Good job!")

	return nil
}
