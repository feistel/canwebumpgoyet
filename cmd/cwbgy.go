package cwbgy

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/feistel/canwebumpgoyet/gitlab"
)

const usage = `Usage:
    canwebumpgoyet [options]

Options:

`

func CLI(args []string) int {
	opts, err := parse(args)
	if err != nil {
		log.Printf("runtime error: %v\n", err)
		return 2
	}

	app := NewApp(opts...)

	if app.cng {
		if err = app.CheckCNG(); err != nil {
			app.logger.Fatalf("error: %v\n", err)
			return 1
		}
	}

	if len(app.projects) != 0 {
		if err = app.CheckProjects(); err != nil {
			app.logger.Fatalf("error: %v\n", err)
			return 1
		}
		return 0
	}

	if err = app.Exec(); err != nil {
		app.logger.Fatalf("error: %v\n", err)
		return 1
	}

	return 0
}

func parse(args []string) ([]AppOption, error) {
	fl := flag.NewFlagSet("canwebumpgoyet", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(fl.Output(), "%s\n", usage)
		fl.PrintDefaults()
		fmt.Fprintln(fl.Output(), "")
	}

	verbose := fl.Bool("v", false, "verbose output: print file url and version information while being fetched")
	filter := Constraint(fl, "filter", *gitlab.SupportedGitlabVersions(), "semver constraint pattern, used to filter gitlab versions")
	m := fl.Bool("main", false, "query main branch status")
	limit := fl.Int("limit", 3, "the maximum number of gitlab releases to query (applied after the filter)")
	projects := fl.String("projects", "", "project ids")
	CNG := fl.Bool("cng", false, "CNG")

	if err := fl.Parse(args); err != nil {
		return nil, err
	}

	l := log.New(os.Stdout, "", 0)

	opts := []AppOption{
		WithLogger(l),
		WithClient(http.DefaultClient),
		WithConstraints(filter),
		WithMainBranch(*m),
		WithLimit(*limit),
		WithVerbose(*verbose),
		WithCNG(*CNG),
	}

	if *projects != "" {
		opts = append(opts, WithProjects(strings.Split(*projects, ",")...))
	}

	return opts, nil
}
