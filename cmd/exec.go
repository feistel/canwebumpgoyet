package cwbgy

import "gitlab.com/feistel/canwebumpgoyet/gitlab"

const goTemplate = `
go version (latest):     %s
go version (additional): %s
`

const gitlabTemplate = `
Gitlab Version: %s
CNG version:                    %-11s (go %s)
Omnibus-builder image revision: %-11s (go %s)
`

func (a *App) Exec() error {
	goCh, goErrCh := a.golangResolver.SupportedVersions()

	var (
		releases []<-chan *gitlab.Release
		errors   []<-chan error
	)

	{
		if a.mainBranch {
			mainCh, mainErrCh := a.gitlabResolver.QueryBranch("master")
			releases = append(releases, mainCh)
			errors = append(errors, mainErrCh)
		}

		glCh, glErrCh := a.gitlabResolver.Query(a.constraints, a.limit)
		releases = append(releases, glCh)
		errors = append(errors, glErrCh)
	}

	select {
	case err := <-goErrCh:
		return err
	case latest := <-goCh:
		additional := <-goCh
		a.logger.Printf(goTemplate, latest, additional)
	}

	for i, rCh := range releases {
		select {
		case err := <-errors[i]:
			return err
		case g := <-rCh:
			a.print(g)

			for r := range rCh {
				a.print(r)
			}
		}
	}

	return nil
}

func (a *App) print(g *gitlab.Release) {
	a.logger.Printf(gitlabTemplate,
		g.Version,
		g.CNG.Version, g.CNG.GoVersion,
		g.Omnibus.BuilderImageRevision, g.Omnibus.BuilderGoVersion)
}
