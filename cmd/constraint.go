package cwbgy

import (
	"flag"

	"github.com/Masterminds/semver/v3"
)

type constraintValue semver.Constraints

func (c *constraintValue) String() string {
	return semver.Constraints(*c).String()
}

func (c *constraintValue) Set(value string) error {
	v, err := semver.NewConstraint(value)
	if err != nil {
		return err
	}

	*c = constraintValue(*v)
	return nil
}

func Constraint(f *flag.FlagSet, name string, value semver.Constraints, usage string) *semver.Constraints {
	p := new(semver.Constraints)
	f.Var(newConstraintValue(value, p), name, usage)
	return p
}

func newConstraintValue(val semver.Constraints, p *semver.Constraints) *constraintValue {
	*p = val
	return (*constraintValue)(p)
}
