package golang

import (
	"encoding/json"
	"fmt"
	"io"

	"github.com/Masterminds/semver/v3"
)

type release struct {
	Version string
}

func (r *Resolver) SupportedVersions() (<-chan *semver.Version, <-chan error) {
	outCh := make(chan *semver.Version, 2)
	errCh := make(chan error, 1)
	go func() {
		resp, err := r.client.Get("https://golang.org/dl/?mode=json")
		if err != nil {
			errCh <- err
			close(errCh)
			return
		}

		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			errCh <- err
			close(errCh)
			return
		}

		var releases []release
		if err := json.Unmarshal(body, &releases); err != nil {
			errCh <- err
			close(errCh)
			return
		}

		if len(releases) != 2 {
			errCh <- fmt.Errorf("malformed releases: expected 2 but got %d", len(releases))
			close(errCh)
			return
		}

		latest, err := semver.NewVersion(releases[0].Version[2:])
		if err != nil {
			errCh <- err
			close(errCh)
			return

		}

		additional, err := semver.NewVersion(releases[1].Version[2:])
		if err != nil {
			errCh <- err
			close(errCh)
			return
		}

		outCh <- latest
		outCh <- additional
		close(outCh)
	}()

	return outCh, errCh

}
