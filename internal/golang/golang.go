package golang

import (
	"log"
	"net/http"
)

type Resolver struct {
	client  *http.Client
	logger  *log.Logger
	verbose bool
}

type Option func(*Resolver)

func NewResolver(opts ...Option) *Resolver {
	var r Resolver
	for _, opt := range opts {
		opt(&r)
	}

	return &r
}

func WithVerbose(verbose bool) Option {
	return func(r *Resolver) {
		r.verbose = verbose
	}
}

func WithClient(client *http.Client) Option {
	return func(r *Resolver) {
		r.client = client
	}
}

func WithLogger(l *log.Logger) Option {
	return func(r *Resolver) {
		r.logger = l
	}
}
