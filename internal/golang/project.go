package golang

import (
	"net/url"

	"github.com/Masterminds/semver/v3"
)

func (r *Resolver) FindVersion(projectid string) (<-chan *semver.Version, <-chan error) {
	outCh := make(chan *semver.Version, 2)
	errCh := make(chan error, 1)

	go func() {
		token := "go "

		line, err := r.findLine("https://gitlab.com/api/v4/projects/"+url.PathEscape(projectid)+"/repository/files/go.mod/raw", token)
		if err != nil {
			errCh <- err
			close(errCh)
			return
		}

		s := line[len(token):]

		v, err := semver.NewVersion(s)
		if err != nil {
			errCh <- err
			close(errCh)
			return
		}

		outCh <- v
		close(outCh)
	}()

	return outCh, errCh
}
