package golang_test

import (
	"log"
	"net/http"
	"testing"

	"gitlab.com/feistel/canwebumpgoyet/internal/golang"
)

func TestLatest(t *testing.T) {
	t.Parallel()

	r := golang.NewResolver(golang.WithClient(http.DefaultClient), golang.WithLogger(log.Default()), golang.WithVerbose(true))
	outCh, errCh := r.SupportedVersions()

	select {
	case err := <-errCh:
		t.Fatal(err)
	case latest := <-outCh:
		old := <-outCh
		if latest.LessThan(old) {
			t.Fatalf("old can't be greater than latest: %v %v", old, latest)
		}
	}
}
