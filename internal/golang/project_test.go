package golang_test

import (
	"log"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/feistel/canwebumpgoyet/internal/golang"
)

func TestGolangProjects(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		id          string
		expectedErr string
		expectedVer *semver.Version
	}{
		"malformed project": {
			id:          "feiste/thisprojectdoesnotexist",
			expectedErr: "no",
		},
		"self": {
			id:          "feistel/canwebumpgoyet",
			expectedVer: semver.MustParse("1.17"),
		},
	}

	for name, tc := range testCases {
		tc := tc

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			r := golang.NewResolver(
				golang.WithClient(http.DefaultClient),
				golang.WithLogger(log.Default()),
			)

			versionsCh, errCh := r.FindVersion(tc.id)

			select {
			case err := <-errCh:
				if tc.expectedErr == "" {
					t.Fatalf("unexpected error while querying: %v", err)
				}

				if !strings.Contains(err.Error(), tc.expectedErr) {
					t.Fatalf("got %v, expected %v", err, tc.expectedErr)
				}
			case v := <-versionsCh:
				if tc.expectedErr != "" {
					t.Fatalf("got %v, expected error", v)
				}

				if !v.Equal(tc.expectedVer) {
					t.Fatalf("got %v, expected %v", *v, *tc.expectedVer)
				}
			case <-time.After(5 * time.Second):
				t.Fatalf("timed out waiting for result")
			}
		})
	}
}
