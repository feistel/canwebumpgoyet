package gitlab_test

import (
	"log"
	"net/http"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/Masterminds/semver/v3"
	g "gitlab.com/feistel/canwebumpgoyet/gitlab"
	"gitlab.com/feistel/canwebumpgoyet/internal/gitlab"
)

func TestGitLabResolver(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		limit          int
		expectedCount  int
		c              *semver.Constraints
		expectedResult []g.Release
		expectedErr    string
		branch         string
	}{
		"small limit": {
			limit:         2,
			expectedCount: 2,
			c:             g.SupportedGitlabVersions(),
		},
		"large limit": {
			limit:         200,
			expectedCount: 1,
			c:             mustParse(t, "= 14.1.0"),
		},
		"unsupported constraint": {
			limit:       200,
			c:           mustParse(t, "= 13.7.0"),
			expectedErr: "not a supported omnibus-builder version",
		},
		"GitLab 14": {
			limit:          200,
			expectedCount:  1,
			c:              mustParse(t, "= 14.0.0"),
			expectedResult: []g.Release{mockGitlab14()},
		},
		"main branch": {
			expectedCount: 1,
			branch:        "master",
		},
		"malformed branch": {
			branch:      "thisbranchdoesnotexist",
			expectedErr: "missing image builder revision",
		},
	}

	for name, tc := range testCases {
		tc := tc

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			r := gitlab.NewResolver(
				gitlab.WithClient(http.DefaultClient),
				gitlab.WithLogger(log.Default()),
				gitlab.WithVerbose(true),
			)

			var versionsCh <-chan *g.Release
			var errCh <-chan error

			if tc.c != nil {
				versionsCh, errCh = r.Query(tc.c, tc.limit)
			} else {
				versionsCh, errCh = r.QueryBranch(tc.branch)
			}

			select {
			case err := <-errCh:
				if tc.expectedErr == "" {
					t.Fatalf("unexpected error while querying: %v", err)
				}

				if !strings.Contains(err.Error(), tc.expectedErr) {
					t.Fatalf("got %v, expected %v", err, tc.expectedErr)
				}
			case v := <-versionsCh:
				if tc.expectedErr != "" {
					t.Fatalf("got %v, expected error", v)
				}

				if tc.expectedResult != nil && !reflect.DeepEqual(*v, tc.expectedResult[0]) {
					t.Fatalf("got %v, expected %v", *v, tc.expectedResult[0])
				}

				got := 1

				for r := range versionsCh {
					if tc.expectedResult != nil && !reflect.DeepEqual(*r, tc.expectedResult[got]) {
						t.Fatalf("got %v, expected %v", *r, tc.expectedResult[got])
					}

					got++
				}

				if got != tc.expectedCount {
					t.Fatalf("got %d versions, expected %d", got, tc.expectedCount)
				}
			case <-time.After(5 * time.Second):
				t.Fatalf("timed out waiting for result")
			}
		})
	}
}

func mustParse(t *testing.T, s string) *semver.Constraints {
	t.Helper()

	c, err := semver.NewConstraint(s)
	if err != nil {
		t.Fatalf("failed to parse constraint: %s", s)
	}
	return c
}

func mockGitlab14() g.Release {
	bir := semver.MustParse("1.6.0")
	goVersion := semver.MustParse("1.16.4")

	return g.Release{
		Version: "v14.0.0-ee",
		Omnibus: &g.OmnibusRelease{
			BuilderImageRevision: bir,
			BuilderGoVersion:     goVersion,
		},
		CNG: &g.CNGRelease{
			Version:   "v14.0.0-ee",
			GoVersion: goVersion,
		},
	}
}
