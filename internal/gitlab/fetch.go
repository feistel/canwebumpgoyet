package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"strings"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/feistel/canwebumpgoyet/gitlab"
)

const (
	// gitlab-org/build/CNG
	CNGVariablesyml = "https://gitlab.com/api/v4/projects/4359271/repository/files/ci_files%2Fvariables.yml/raw?ref="

	// gitlab-org/omnibus-gitlab
	OmnibusGitlabCIyml = "https://gitlab.com/api/v4/projects/20699/repository/files/.gitlab-ci.yml/raw?ref="

	// gitlab-org/gitlab-omnibus-builder
	OmnibusBuilderDockerVersions = "https://gitlab.com/api/v4/projects/145205/repository/files/docker%2FVERSIONS/raw?ref="
)

type tag struct {
	Name   string `json:"tag_name"`
	branch string
}

func (r *Resolver) QueryBranch(name string) (<-chan *gitlab.Release, <-chan error) {
	t := tag{
		Name:   name,
		branch: name,
	}
	return r.newGitlabRelease(t)
}

func (r *Resolver) Query(constraints *semver.Constraints, limit int) (<-chan *gitlab.Release, <-chan error) {
	outCh := make(chan *gitlab.Release)
	outErrCh := make(chan error, 1)

	go func() {
		tags, err := r.fetchGitTags(constraints, limit)
		if err != nil {
			outErrCh <- fmt.Errorf("failed to fetch git tags: %w", err)
			close(outErrCh)
			return
		}

		if r.verbose {
			r.logger.Printf("retrieved %d gitlab versions: %v", len(tags), tags)
		}

		var releaseChArr []<-chan *gitlab.Release
		var errChArr []<-chan error

		for _, t := range tags {
			releaseCh, errCh := r.newGitlabRelease(t)

			releaseChArr = append(releaseChArr, releaseCh)
			errChArr = append(errChArr, errCh)
		}

		result := make([]*gitlab.Release, len(releaseChArr), len(releaseChArr))

		for i, relCh := range releaseChArr {
			select {
			case err := <-errChArr[i]:
				outErrCh <- err
				close(outErrCh)
				return
			case rel := <-relCh:
				result[i] = rel
			}
		}

		for _, res := range result {
			outCh <- res
		}
		close(outCh)
	}()

	return outCh, outErrCh
}

func (r *Resolver) fetchGitTags(constraints *semver.Constraints, limit int) ([]tag, error) {
	resp, err := r.okGet("https://gitlab.com/api/v4/projects/278964/releases")
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var tags []tag
	if err := json.Unmarshal(body, &tags); err != nil {
		return nil, err
	}

	result := r.filterTags(tags, constraints)

	if len(result) > limit {
		result = result[0:limit]
	}

	return result, nil
}

func (r *Resolver) findCNGGoVersion(t tag) (<-chan *gitlab.CNGRelease, <-chan error) {
	outCh := make(chan *gitlab.CNGRelease, 1)
	outErrCh := make(chan error, 1)

	go func() {
		token := "  GO_VERSION: "
		line, err := r.findLine(CNGVariablesyml+url.QueryEscape(t.Name), token)
		if err != nil {
			outErrCh <- fmt.Errorf("failed to find CNG Go version for %s: %w", t.Name, err)
			close(outErrCh)
			return
		}

		v := strings.Trim(line[len(token):], "\"")

		if r.verbose {
			r.logger.Printf("==> found CNG go version %s for %s", v, t.Name)
		}

		sv, err := semver.NewVersion(v)
		if err != nil {
			outErrCh <- fmt.Errorf("malformed CNG Go version for %s: %w", t.Name, err)
			close(outErrCh)
			return
		}

		outCh <- &gitlab.CNGRelease{
			Version:   t.Name,
			GoVersion: sv,
		}
		close(outCh)
	}()

	return outCh, outErrCh
}

func (r *Resolver) findBuilderImageRevision(t tag) (<-chan *semver.Version, <-chan error) {
	outCh := make(chan *semver.Version, 1)
	outErrCh := make(chan error, 1)

	go func() {
		branch := t.branch
		if branch == "" {
			branch = t.Name[1:len(t.Name)-3] + "+ee.0"
		}

		token := "  BUILDER_IMAGE_REVISION: \""
		line, err := r.findLine(OmnibusGitlabCIyml+url.QueryEscape(branch), token)
		if err != nil {
			outErrCh <- fmt.Errorf("missing image builder revision for %s: %w", branch, err)
			close(outErrCh)
			return
		}

		v := line[len(token) : len(line)-1]

		if r.verbose {
			r.logger.Printf("==> found builder image revision %s for %s", v, branch)
		}

		sv, err := semver.NewVersion(v)
		if err != nil {
			outErrCh <- fmt.Errorf("malformed image builder revision for %s: %w", branch, err)
			close(outErrCh)
			return
		}

		outCh <- sv
		close(outCh)
	}()

	return outCh, outErrCh
}

func (r *Resolver) findBuilderGoVersion(bir *semver.Version) (*semver.Version, error) {
	if ok, err := gitlab.SupportedOmnibusVersions().Validate(bir); !ok {
		return nil, fmt.Errorf("%s is not a supported omnibus-builder version: %v", bir, err)
	}

	token := "GO_VERSION="
	line, err := r.findLine(OmnibusBuilderDockerVersions+url.QueryEscape(bir.String()), token)
	if err != nil {
		return nil, fmt.Errorf("failed to find Builder Go version for %s: %w", bir, err)
	}

	v := line[len(token):]

	if r.verbose {
		r.logger.Printf("==> found builder go version %s for %s", v, bir)
	}
	return semver.NewVersion(v)
}

func (r *Resolver) newGitlabRelease(t tag) (<-chan *gitlab.Release, <-chan error) {
	outCh := make(chan *gitlab.Release, 1)
	outErrCh := make(chan error, 1)

	go func() {
		birCh, birErrCh := r.findBuilderImageRevision(t)
		cgvCh, cgvErrCh := r.findCNGGoVersion(t)

		var bir *semver.Version
		select {
		case err := <-birErrCh:
			outErrCh <- err
			close(outErrCh)
			return
		case bir = <-birCh:
		}

		var cgv *gitlab.CNGRelease
		select {
		case err := <-cgvErrCh:
			outErrCh <- err
			close(outErrCh)
			return
		case cgv = <-cgvCh:
		}

		bgv, err := r.findBuilderGoVersion(bir)
		if err != nil {
			outErrCh <- err
			close(outErrCh)
			return
		}

		release := gitlab.Release{
			Version: t.Name,
			Omnibus: &gitlab.OmnibusRelease{
				BuilderImageRevision: bir,
				BuilderGoVersion:     bgv,
			},
			CNG: cgv,
		}

		outCh <- &release
		close(outCh)
	}()

	return outCh, outErrCh
}

func (r *Resolver) filterTags(tags []tag, c *semver.Constraints) []tag {
	var result []tag
	for _, t := range tags {
		v, err := semver.NewVersion(t.Name[1 : len(t.Name)-3])
		if err != nil {
			r.logger.Printf("skipping invalid version %s: %v", t.Name, err)
			continue
		}

		ok, errs := c.Validate(v)
		if ok {
			result = append(result, t)
		} else if r.verbose {
			r.logger.Printf("ignoring version %s: %v", t.Name, errs)
		}
	}
	return result
}
