package gitlab

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"strings"
)

func (r *Resolver) findLine(url string, prefix string) (string, error) {
	rsp, err := r.okGet(url)
	if err != nil {
		return "", fmt.Errorf("failed to find line %s: %w", prefix, err)
	}

	defer rsp.Body.Close()

	l, ok := findLine(prefix, rsp.Body)
	if !ok {
		return "", fmt.Errorf("line not found: %s", prefix)
	}

	return l, nil
}

func (r *Resolver) okGet(url string) (*http.Response, error) {
	resp, err := r.client.Get(url)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("url %s returned status code %s", url, resp.Status)
	}

	return resp, nil
}

func findLine(prefix string, body io.ReadCloser) (string, bool) {
	scanner := bufio.NewScanner(body)

	for scanner.Scan() {
		s := scanner.Text()
		if strings.HasPrefix(s, prefix) {
			return s, true
		}
	}
	return "", false
}
