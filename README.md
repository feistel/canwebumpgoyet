# canwebumpgoyet: cli tool to improve handling of gitlab go version policy

## Introduction

Gitlab has a policy for supporting [multiple Go versions](https://docs.gitlab.com/ee/development/go_guide/#supporting-multiple-go-versions) due to backwards compatibility and/or in case they need to backport some security fixes.  
This is a cli tool to reduce the burden of keeping track of multiple versions on multiple branches, ensure the policy is respected and spot inconsistencies (e.g. release having multiple go versions).

## Run

`go run gitlab.com/feistel/canwebumpgoyet@latest`

## Usage

```
canwebumpgoyet -h
Usage:
   canwebumpgoyet [options]

 Options:
   -cng
         CNG
   -filter value
         semver constraint pattern, used to filter gitlab versions (default >=13.9.0)
   -limit int
         the maximum number of gitlab releases to query (applied after the filter) (default 3)
   -main
         query main branch status
   -projects string
         project ids
   -v    verbose output: print file url and version information while being fetche
```
