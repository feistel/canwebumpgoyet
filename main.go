package main

import (
	"os"

	cwbgy "gitlab.com/feistel/canwebumpgoyet/cmd"
)

func main() {
	os.Exit(cwbgy.CLI(os.Args[1:]))
}
